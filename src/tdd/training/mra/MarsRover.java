package tdd.training.mra;

import java.util.List;

public class MarsRover {
	private int planetX;
	private int planetY;
	private int positionX;
	private int positionY;
	private String direction;
	private List<String> planetObstacles;
	/**
	 * It initializes the rover at the coordinates (0,0), facing North, on a planet
	 * (represented as a grid with x and y coordinates) containing obstacles.
	 * 
	 * @param planetX         The x dimension of the planet.
	 * @param planetY         The y dimension of the planet.
	 * @param planetObstacles The obstacles on the planet. Each obstacle is a string
	 *                        (without white spaces) formatted as follows:
	 *                        "(oi_x,oi_y)". <code>null</code> if the planet does
	 *                        not contain obstacles.
	 * 
	 * @throws MarsRoverException
	 */
	public MarsRover(int planetX, int planetY, List<String> planetObstacles) throws MarsRoverException {
		this.planetX=planetX;
		this.planetY=planetY;
		this.planetObstacles=planetObstacles;
		direction="N";
	}

	/**
	 * It returns whether, or not, the planet (where the rover moves) contains an
	 * obstacle in a cell.
	 * 
	 * @param x The x coordinate of the cell
	 * @param y The y coordinate of the cell
	 * @return <true> if the cell contains an obstacle, <false> otherwise.
	 * @throws MarsRoverException
	 */
	public boolean planetContainsObstacleAt(int x, int y) throws MarsRoverException {
		int size=0;
		size=planetObstacles.size();
		String coord;
		coord= "("+String.valueOf(x)+","+String.valueOf(y)+")";
		for(int i=0;i<size;i++) {
			if(planetObstacles.get(i).equals(coord)) {
				return true;
			}
		}
		return false;
	}

	/**
	 * It lets the rover move on the planet according to a command string. The
	 * return string contains the new position of the rover, its direction, and the
	 * obstacles it has encountered while moving on the planet (if any).
	 * 
	 * @param commandString A string that can contain a single command -- i.e. "f"
	 *                      (forward), "b" (backward), "l" (left), or "r" (right) --
	 *                      or a combination of single commands.
	 * @return The return string that contains the position and direction of the
	 *         rover, and the obstacles the rover has encountered while moving on
	 *         the planet (if any). The return string (without white spaces) has the
	 *         following format: "(x,y,dir)(o1_x,o1_y)(o2_x,o2_y)...(on_x,on_y)". x
	 *         and y define the new position of the rover while dir represents its
	 *         direction (i.e., N, S, W, or E). Finally, oi_x and oi_y are the
	 *         coordinates of the i-th encountered obstacle.
	 * @throws MarsRoverException
	 */
	public String executeCommand(String commandString) throws MarsRoverException {
		String primitiveCommand;
		primitiveCommand=commandString;
		String prevPosition;
		String Position = new String();
		if(commandString.equals("")) {
			 positionX=0;
			 positionY=0;
			 direction="N";
			 return "("+String.valueOf(positionX)+","+String.valueOf(positionY)+","+direction+")";

		}
		for(int i=0;i<primitiveCommand.length();i++) {
			prevPosition="("+String.valueOf(positionX)+","+String.valueOf(positionY)+","+direction+")";
			
			commandString=Character.toString(primitiveCommand.charAt(i));
			
			
			if(commandString.equals("r"))
				right();
			if(commandString.equals("l"))
				left();
			if(commandString.equals("f")) 	
				forward();
			if(commandString.equals("b"))
				backward();
				
			if(positionX==-1)
				positionX=planetX-1;
			else if(positionX>planetX)
				positionX=0;
			
			if(positionY==-1)
				positionY=planetY-1;
			else if(positionY>planetY)
				positionY=0;
			
			for(int j=0;j<planetObstacles.size();j++) {
				if(planetObstacles.get(j).equals("("+String.valueOf(positionX)+","+String.valueOf(positionY)+")")) {
					String obstacle;
					obstacle=planetObstacles.get(j);
					return prevPosition+obstacle ;
				}
			}
			Position="("+String.valueOf(positionX)+","+String.valueOf(positionY)+","+direction+")";
		}
		
		
		
		
		return Position;
	}

	private void backward() {

		if(direction.equals("N"))
			positionY--;
		else if(direction.equals("W"))
			positionX++;
		else if(direction.equals("S"))
			positionY++;
		else if(direction.equals("E"))
			positionX--;
	}

	private void forward() {
		if(direction.equals("N"))
			positionY++;
		else if(direction.equals("W"))
			positionX--;
		else if(direction.equals("S"))
			positionY--;
		else if(direction.equals("E"))
			positionX++;
	}

	private void left() {
		if(direction.equals("N"))
			direction="W";
		else if(direction.equals("W"))
			direction="S";
		else if(direction.equals("S"))
			direction="E";
		else if(direction.equals("E"))
			direction="N";
		
	}

	private void right() {
		if(direction.equals("N"))
			direction="E";
		else if(direction.equals("E"))
			direction="S";
		else if(direction.equals("S"))
			direction="W";
		else if(direction.equals("W"))
			direction="N";
	}

}

package tdd.training.mra;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

public class MarsRoverTest {

	@Test
	public void testIfThereAreObstaclesAt() throws MarsRoverException {
		List<String> planetObstacles = new ArrayList<>();
		planetObstacles.add("(4,7)");
		planetObstacles.add("(2,3)");
		MarsRover rover = new MarsRover(10, 10, planetObstacles);
		assertTrue(rover.planetContainsObstacleAt(4,7));
		assertTrue(rover.planetContainsObstacleAt(2,3));

	}
	@Test
	public void testInitialDirectionAndPosition() throws MarsRoverException {
		List<String> planetObstacles = new ArrayList<>();
		MarsRover rover = new MarsRover(10, 10, planetObstacles);
		assertEquals(rover.executeCommand(""),("(0,0,N)"));

	}
	@Test
	public void testChangeDirectionToRight() throws MarsRoverException {
		List<String> planetObstacles = new ArrayList<>();
		planetObstacles.add("(4,7)");
		planetObstacles.add("(2,3)");
		MarsRover rover = new MarsRover(10, 10, planetObstacles);
		assertEquals(rover.executeCommand("r"),("(0,0,E)"));

	}
	@Test
	public void testChangeDirectionToLeft() throws MarsRoverException {
		List<String> planetObstacles = new ArrayList<>();
		planetObstacles.add("(4,7)");
		planetObstacles.add("(2,3)");
		MarsRover rover = new MarsRover(10, 10, planetObstacles);
		assertEquals(rover.executeCommand("l"),("(0,0,W)"));

	}
	@Test
	public void testForward() throws MarsRoverException {
		List<String> planetObstacles = new ArrayList<>();
		planetObstacles.add("(4,7)");
		planetObstacles.add("(2,3)");
		MarsRover rover = new MarsRover(10, 10, planetObstacles);
		assertEquals(rover.executeCommand("f"),("(0,1,N)"));

	}
	@Test
	public void testBackward() throws MarsRoverException {
		List<String> planetObstacles = new ArrayList<>();
		planetObstacles.add("(4,7)");
		planetObstacles.add("(2,3)");
		MarsRover rover = new MarsRover(10, 10, planetObstacles);
		rover.executeCommand("f");
		assertEquals(rover.executeCommand("b"),("(0,0,N)"));

	}
	@Test
	public void testCombinesCommnads() throws MarsRoverException {
		List<String> planetObstacles = new ArrayList<>();
		planetObstacles.add("(4,7)");
		planetObstacles.add("(2,3)");
		MarsRover rover = new MarsRover(10, 10, planetObstacles);
		
		assertEquals(rover.executeCommand("ffrff"),("(2,2,E)"));

	}
	@Test
	public void testWrapping() throws MarsRoverException {
		List<String> planetObstacles = new ArrayList<>();
		planetObstacles.add("(4,7)");
		planetObstacles.add("(2,3)");
		MarsRover rover = new MarsRover(10, 10, planetObstacles);
		
		assertEquals(rover.executeCommand("b"),("(0,9,N)"));

	}
	
	@Test
	public void testSingleObstacle() throws MarsRoverException {
		List<String> planetObstacles = new ArrayList<>();
		planetObstacles.add("(2,2)");
		MarsRover rover = new MarsRover(10, 10, planetObstacles);
		
		assertEquals(rover.executeCommand("ffrfff"),("(1,2,E)(2,2)"));

	}
}
